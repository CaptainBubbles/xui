﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;

namespace Xui
{
    public class XuiManager
    {
        #region Private Fields

        private List<XuiControl> _managedControls;
        private Random _rng;
        private Microsoft.Xna.Framework.Input.MouseState _mouse,_oldMouse;
        private Microsoft.Xna.Framework.Input.KeyboardState _keyboard,_oldKeyboard;
        private bool _firstFrame;

        #endregion

        #region Public Properties

        public int ControlCount { get { return _managedControls.Count; } }
        public Microsoft.Xna.Framework.Input.MouseState Mouse { get { return _mouse; } }
        public XuiControl ControlWithFocus
        {
            get
            {
                foreach (XuiControl c in _managedControls)
                {
                    if (c.HasFocus)
                        return c;
                }
                return null;
            }
        }

        #endregion

        #region Contructor

        public XuiManager()
        {
            _rng = new Random();
            _managedControls = new List<XuiControl>();
            _firstFrame = true;
        }

        #endregion

        #region Public Methods

        public void AddControl(XuiControl control)
        {
            control.Id = createId();
            _managedControls.Add(control);
        }

        public void Update(Microsoft.Xna.Framework.Input.MouseState ms,Microsoft.Xna.Framework.Input.KeyboardState ks)
        {            
            _mouse = ms;
            _keyboard = ks;

            if(!_firstFrame)
                handleInput();

            _oldMouse = _mouse;
            _oldKeyboard = _keyboard;
            _firstFrame = false;
        }

        #endregion

        #region Private Methods

        private int createId()
        {
            bool valid = false;
            int id = -1;
            while (!valid)
            {
                id = _rng.Next(int.MaxValue - 1);
                valid = true;

                foreach (XuiControl xc in _managedControls)
                {
                    if (xc.Id == id)
                        valid = false;
                }
            }
            return id;
        }

        private void handleInput()
        {
            int scrolling = 0;
            if(_oldMouse.ScrollWheelValue < _mouse.ScrollWheelValue)
                scrolling = -1;
            else if(_mouse.ScrollWheelValue < _oldMouse.ScrollWheelValue)
                scrolling = 1;            
            
            //Clicked Event (responds to LMB / Space / Enter
            if (ButtonPressed("Left") || KeyPressed(Microsoft.Xna.Framework.Input.Keys.Enter) 
                || KeyPressed(Microsoft.Xna.Framework.Input.Keys.Space))
            {
                for (int i = 0; i < _managedControls.Count; i++)
                {
                    //Invoke event handler if control was clicked
                    XuiControl xc = _managedControls[i];
                    if (xc.BoundingBox.Contains(new Point(_mouse.X, _mouse.Y)))
                    {
                        if (xc.Clicked != null)
                            xc.Clicked(_mouse, null);
                        i = _managedControls.Count;
                    }
                }
            }

            //Right Clicked Event
            if (ButtonPressed("Right"))
            {
                for (int i = 0; i < _managedControls.Count; i++)
                {
                    //Invoke event handler if control was clicked
                    XuiControl xc = _managedControls[i];
                    if (xc.BoundingBox.Contains(new Point(_mouse.X, _mouse.Y)))
                    {
                        if (xc.RightClicked != null)
                            xc.RightClicked(_mouse, null);
                        i = _managedControls.Count;
                    }
                }
            }

            //Middle Clicked Event
            if (ButtonPressed("Middle"))
            {
                for (int i = 0; i < _managedControls.Count; i++)
                {
                    //Invoke event handler if control was clicked
                    XuiControl xc = _managedControls[i];
                    if (xc.BoundingBox.Contains(new Point(_mouse.X, _mouse.Y)))
                    {
                        if (xc.MiddleClicked != null)
                            xc.MiddleClicked(_mouse, null);
                        i = _managedControls.Count;
                    }
                }
            }

            //Mouse Enter
            foreach (XuiControl xc in _managedControls)
            {
                if (xc.BoundingBox.Contains(new Point(_mouse.X, _mouse.Y)) && !xc.Hovered)
                    xc.MouseEnter(_mouse, null);
            }
            //Mouse Leave
            foreach (XuiControl xc in _managedControls)
            {
                if (!xc.BoundingBox.Contains(new Point(_mouse.X, _mouse.Y)) && xc.Hovered)
                    xc.MouseLeave(_mouse, null);
            }
            

        }

        public bool KeyPressed(Microsoft.Xna.Framework.Input.Keys key)
        {
            return (_keyboard.IsKeyDown(key) && !_oldKeyboard.IsKeyDown(key));
        }

        /// <summary>
        /// Checks if the specified button has just been pressed 
        /// Valid button names : "Left", "Right", "Middle"
        /// </summary>       
        public bool ButtonPressed(string button)
        {
            switch (button)
            {
                case "Left":
                    return (_mouse.LeftButton == Microsoft.Xna.Framework.Input.ButtonState.Released
                        && _oldMouse.LeftButton == Microsoft.Xna.Framework.Input.ButtonState.Pressed);
                case "Right":
                    return (_mouse.RightButton == Microsoft.Xna.Framework.Input.ButtonState.Released
                        && _oldMouse.RightButton == Microsoft.Xna.Framework.Input.ButtonState.Pressed);
                case "Middle":
                    return (_mouse.MiddleButton == Microsoft.Xna.Framework.Input.ButtonState.Released
                        && _oldMouse.MiddleButton == Microsoft.Xna.Framework.Input.ButtonState.Pressed);
                default:
                    return false;
            }
        }

        #endregion

    }
}
