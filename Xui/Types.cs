﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Xui
{
    /// <summary>
    /// Describes the control's type 
    /// </summary>
    public enum ControlTypes
    {
        /// <summary>
        /// Toggleable controls like buttons, check boxes, radio buttons,etc
        /// </summary>
        BINARY,
        /// <summary>
        /// Analog controls like sliders
        /// </summary>
        ANALOG,
        /// <summary>
        /// Textboxes, labels,etc
        /// </summary>
        TEXT,
        MISC
    }
}
