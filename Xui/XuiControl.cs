﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Xui
{
    public abstract class XuiControl
    {
        #region Private Fields

        private Vector2 _position, _scale;
        private int _id;
        private ControlTypes _type;
        private bool _hovered;

        #endregion

        #region Public Fields

        public bool Active, Hidden, Responsive, FlagDeletion, HasFocus;
        /// <summary>
        /// A non unique name for this control
        /// </summary>
        public string Name;

        #endregion

        #region Constructor

        public XuiControl()
        {
            MouseEnter += hoverEnter;
            MouseLeave += hoverLeave;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// A unique identifier for this control
        /// </summary>
        public int Id { get { return _id; } set { _id = value; } }
        public ControlTypes Type { get { return _type; } }
        public bool Hovered { get { return _hovered; } }
        public virtual Vector2 Position
        {
            get { return _position; }
            set { _position = value; }
        }
        public virtual Vector2 Scale
        {
            get { return _scale; }
            set
            {
                if (value.X > 0 && value.Y > 0)
                    _scale = value;
            }
        }
        public virtual Rectangle BoundingBox
        {
            get
            {
                return new Rectangle((int)_position.X,
                    (int)_position.Y, (int)_scale.X, (int)_scale.Y);
            }
        }

        #endregion

        #region Public Methods

        public XuiControl(Vector2 pos, ControlTypes type)
        {
            _position = pos;
            Active = true;
            Responsive = true;
            _type = type;
        }

        public virtual void Update() { }

        public virtual void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb) { }

        public virtual void OnRemoved() { }

        #endregion

        #region Event Handlers

        public System.EventHandler Clicked;
        public System.EventHandler RightClicked;
        public System.EventHandler MiddleClicked;
        public System.EventHandler Scrolled;
        public System.EventHandler MouseEnter;
        public System.EventHandler MouseLeave;

        private void hoverEnter(object sender, EventArgs args)
        {
            _hovered = true;
        }
        private void hoverLeave(object sender, EventArgs args)
        {
            _hovered = false;
        }

        #endregion
    }
}
